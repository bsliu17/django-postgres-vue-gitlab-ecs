import * as dotenv from "dotenv";
import { prisma, PrismaClient } from "@prisma/client";
import { Salt, parseSalt } from "../src/auth/password.service";
import { hash } from "bcrypt";
const Faker = require('faker')

if (require.main === module) {
  dotenv.config();

  const { BCRYPT_SALT } = process.env;

  if (!BCRYPT_SALT) {
    throw new Error("BCRYPT_SALT environment variable must be defined");
  }

  const salt = parseSalt(BCRYPT_SALT);

  seed(salt).catch((error) => {
    console.error(error);
    process.exit(1);
  });
}

async function seed(bcryptSalt: Salt) {
  console.info("Seeding database...");

  const client = new PrismaClient();
  // const data = {
  //   username: "admin",
  //   password: await hash("admin", bcryptSalt),
  //   roles: ["user"],
  // };
  await Promise.all(Array.from(Array(10).keys()).map(async (elem, idx) => {
    const d = { 
      username: Faker.internet.userName() + idx, 
      password: await hash("admin", bcryptSalt),
      roles: ["user"],
    } 
    return client.user.upsert({
      where: { username: d.username} ,
      update: {},
      create: d 
    })
  }))
  // await client.user.upsert({
  //   where: { username: data.username },
  //   update: {},
  //   create: data,
  // });
  client.$disconnect();
  console.info("Seeded database successfully");
}
