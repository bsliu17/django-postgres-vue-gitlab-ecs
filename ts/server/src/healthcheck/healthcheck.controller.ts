import * as common from "@nestjs/common";
import * as swagger from "@nestjs/swagger";
import * as nestAccessControl from "nest-access-control";
import { HealthCheckService } from "./healthCheck.service";
import { HealthCheckControllerBase } from "./base/healthCheck.controller.base";

@swagger.ApiBasicAuth()
@swagger.ApiTags("health-check")
@common.Controller("health-check")
export class HealthCheckController extends HealthCheckControllerBase {
  constructor(
    protected readonly service: HealthCheckService,
  ) {
    super(service);
  }
}
