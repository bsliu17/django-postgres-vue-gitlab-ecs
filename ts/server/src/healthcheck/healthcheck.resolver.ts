import * as common from "@nestjs/common";
import * as graphql from "@nestjs/graphql";
import * as nestAccessControl from "nest-access-control";
import * as gqlBasicAuthGuard from "../auth/gqlBasicAuth.guard";
import * as gqlACGuard from "../auth/gqlAC.guard";
import { HealthCheckResolverBase } from "./base/healthCheck.resolver.base";
// import { HealthCheck } from "./base/healthCheck";
import { HealthCheckService } from "./healthCheck.service";

@graphql.Resolver(() => Boolean)
@common.UseGuards(gqlBasicAuthGuard.GqlBasicAuthGuard, gqlACGuard.GqlACGuard)
export class HealthCheckResolver extends HealthCheckResolverBase {
  constructor(
    protected readonly service: HealthCheckService,
    @nestAccessControl.InjectRolesBuilder()
    protected readonly rolesBuilder: nestAccessControl.RolesBuilder
  ) {
    super(service, rolesBuilder);
  }
}
