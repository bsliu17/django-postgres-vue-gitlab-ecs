import { Injectable } from "@nestjs/common";
import { PrismaService } from "nestjs-prisma";
import { HealthCheckServiceBase } from "./base/healthCheck.service.base";

@Injectable()
export class HealthCheckService extends HealthCheckServiceBase {
  constructor(protected readonly prisma: PrismaService) {
    super(prisma);
  }
}

// hello