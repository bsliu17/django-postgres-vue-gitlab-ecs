import { PrismaService } from "nestjs-prisma";
import { Prisma } from "@prisma/client";

export class HealthCheckServiceBase {
  constructor(protected readonly prisma: PrismaService) {}

  // async count<T extends Prisma.healthCheckFindManyArgs>(
  //   args: Prisma.SelectSubset<T, Prisma.healthCheckFindManyArgs>
  // ): Promise<number> {
  //   return this.prisma.healthCheck.count(args);
  // }

  // async findMany<T extends Prisma.healthCheckFindManyArgs>(
  //   args: Prisma.SelectSubset<T, Prisma.healthCheckFindManyArgs>
  // ): Promise<healthCheck[]> {
  //   return this.prisma.healthCheck.findMany(args);
  // }
  async getHealthCheck(): Promise<boolean> {
    return true;
  }
  // async create<T extends Prisma.healthCheckCreateArgs>(
  //   args: Prisma.SelectSubset<T, Prisma.healthCheckCreateArgs>
  // ): Promise<healthCheck> {
  //   return this.prisma.healthCheck.create<T>(args);
  // }
  // async update<T extends Prisma.healthCheckUpdateArgs>(
  //   args: Prisma.SelectSubset<T, Prisma.healthCheckUpdateArgs>
  // ): Promise<healthCheck> {
  //   return this.prisma.healthCheck.update<T>(args);
  // }
  // async delete<T extends Prisma.healthCheckDeleteArgs>(
  //   args: Prisma.SelectSubset<T, Prisma.healthCheckDeleteArgs>
  // ): Promise<healthCheck> {
  //   return this.prisma.healthCheck.delete(args);
  // }

  // async findOrders(
  //   parentId: string,
  //   args: Prisma.OrderFindManyArgs
  // ): Promise<Order[]> {
  //   return this.prisma.healthCheck
  //     .findUnique({
  //       where: { id: parentId },
  //     })
  //     .orders(args);
  // }
}
