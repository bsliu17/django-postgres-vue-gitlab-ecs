import * as common from "@nestjs/common";
import * as swagger from "@nestjs/swagger";
import * as nestMorgan from "nest-morgan";
import * as nestAccessControl from "nest-access-control";
import * as basicAuthGuard from "../../auth/basicAuth.guard";
import * as abacUtil from "../../auth/abac.util";
import { isRecordNotFoundError } from "../../prisma.util";
import * as errors from "../../errors";
import { Request } from "express";
import { plainToClass } from "class-transformer";
import { HealthCheckService } from "../healthCheck.service";
// import { healthCheckCreateInput } from "./healthCheckCreateInput";
// import { healthCheckWhereInput } from "./healthCheckWhereInput";
// import { healthCheckWhereUniqueInput } from "./healthCheckWhereUniqueInput";
// import { healthCheckFindManyArgs } from "./healthCheckFindManyArgs";
// import { healthCheckUpdateInput } from "./healthCheckUpdateInput";
// import { healthCheck } from "./healthCheck";
// import { OrderWhereInput } from "../../order/base/OrderWhereInput";
// import { Order } from "../../order/base/Order";

export class HealthCheckControllerBase {
  constructor(
    protected readonly service: HealthCheckService,
    // protected readonly rolesBuilder: nestAccessControl.RolesBuilder
  ) {}

  // @common.UseInterceptors(nestMorgan.MorganInterceptor("combined"))
  // @common.UseGuards(basicAuthGuard.BasicAuthGuard, nestAccessControl.ACGuard)
  // @common.Post()
  // @nestAccessControl.UseRoles({
  //   resource: "healthCheck",
  //   action: "create",
  //   possession: "any",
  // })
  // @swagger.ApiCreatedResponse({ type: healthCheck })
  // @swagger.ApiForbiddenResponse({ type: errors.ForbiddenException })
  // async create(
  //   @common.Body() data: healthCheckCreateInput,
  //   @nestAccessControl.UserRoles() userRoles: string[]
  // ): Promise<healthCheck> {
  //   const permission = this.rolesBuilder.permission({
  //     role: userRoles,
  //     action: "create",
  //     possession: "any",
  //     resource: "healthCheck",
  //   });
  //   const invalidAttributes = abacUtil.getInvalidAttributes(permission, data);
  //   if (invalidAttributes.length) {
  //     const properties = invalidAttributes
  //       .map((attribute: string) => JSON.stringify(attribute))
  //       .join(", ");
  //     const roles = userRoles
  //       .map((role: string) => JSON.stringify(role))
  //       .join(",");
  //     throw new errors.ForbiddenException(
  //       `providing the properties: ${properties} on ${"healthCheck"} creation is forbidden for roles: ${roles}`
  //     );
  //   }
  //   return await this.service.create({
  //     data: data,
  //     select: {
  //       createdAt: true,
  //       description: true,
  //       id: true,
  //       itemPrice: true,
  //       name: true,
  //       updatedAt: true,
  //     },
  //   });
  // }

  // @common.UseInterceptors(nestMorgan.MorganInterceptor("combined"))
  // @common.UseGuards(basicAuthGuard.BasicAuthGuard, nestAccessControl.ACGuard)
  // @common.Get()
  // @nestAccessControl.UseRoles({
  //   resource: "healthCheck",
  //   action: "read",
  //   possession: "any",
  // })
  // @swagger.ApiOkResponse({ type: [healthCheck] })
  // @swagger.ApiForbiddenResponse()
  // @swagger.ApiQuery({
  //   type: () => healthCheckFindManyArgs,
  //   style: "deepObject",
  //   explode: true,
  // })
  // async findMany(
  //   @common.Req() request: Request,
  //   @nestAccessControl.UserRoles() userRoles: string[]
  // ): Promise<healthCheck[]> {
  //   const args = plainToClass(healthCheckFindManyArgs, request.query);

  //   const permission = this.rolesBuilder.permission({
  //     role: userRoles,
  //     action: "read",
  //     possession: "any",
  //     resource: "healthCheck",
  //   });
  //   const results = await this.service.findMany({
  //     ...args,
  //     select: {
  //       createdAt: true,
  //       description: true,
  //       id: true,
  //       itemPrice: true,
  //       name: true,
  //       updatedAt: true,
  //     },
  //   });
  //   return results.map((result) => permission.filter(result));
  // }

  // @common.UseInterceptors(nestMorgan.MorganInterceptor("combined"))
  // @common.UseGuards(basicAuthGuard.BasicAuthGuard, nestAccessControl.ACGuard)
  
  // @nestAccessControl.UseRoles({
  //   resource: "healthCheck",
  //   action: "read",
  //   possession: "own",
  // })
  // @swagger.ApiOkResponse({ type: healthCheck })
  // @swagger.ApiNotFoundResponse({ type: errors.NotFoundException })
  // @swagger.ApiForbiddenResponse({ type: errors.ForbiddenException })
  @common.Get("/")
  async getHealthCheck(): Promise<boolean> {
    // const permission = this.rolesBuilder.permission({
    //   role: userRoles,
    //   action: "read",
    //   possession: "own",
    //   resource: "healthCheck",
    // });
    const result = await this.service.getHealthCheck()
    return result
  }

  // @common.UseInterceptors(nestMorgan.MorganInterceptor("combined"))
  // @common.UseGuards(basicAuthGuard.BasicAuthGuard, nestAccessControl.ACGuard)
  // @common.Patch("/:id")
  // @nestAccessControl.UseRoles({
  //   resource: "healthCheck",
  //   action: "update",
  //   possession: "any",
  // })
  // @swagger.ApiOkResponse({ type: healthCheck })
  // @swagger.ApiNotFoundResponse({ type: errors.NotFoundException })
  // @swagger.ApiForbiddenResponse({ type: errors.ForbiddenException })
  // async update(
  //   @common.Param() params: healthCheckWhereUniqueInput,
  //   @common.Body()
  //   data: healthCheckUpdateInput,
  //   @nestAccessControl.UserRoles() userRoles: string[]
  // ): Promise<healthCheck | null> {
  //   const permission = this.rolesBuilder.permission({
  //     role: userRoles,
  //     action: "update",
  //     possession: "any",
  //     resource: "healthCheck",
  //   });
  //   const invalidAttributes = abacUtil.getInvalidAttributes(permission, data);
  //   if (invalidAttributes.length) {
  //     const properties = invalidAttributes
  //       .map((attribute: string) => JSON.stringify(attribute))
  //       .join(", ");
  //     const roles = userRoles
  //       .map((role: string) => JSON.stringify(role))
  //       .join(",");
  //     throw new errors.ForbiddenException(
  //       `providing the properties: ${properties} on ${"healthCheck"} update is forbidden for roles: ${roles}`
  //     );
  //   }
  //   try {
  //     return await this.service.update({
  //       where: params,
  //       data: data,
  //       select: {
  //         createdAt: true,
  //         description: true,
  //         id: true,
  //         itemPrice: true,
  //         name: true,
  //         updatedAt: true,
  //       },
  //     });
  //   } catch (error) {
  //     if (isRecordNotFoundError(error)) {
  //       throw new errors.NotFoundException(
  //         `No resource was found for ${JSON.stringify(params)}`
  //       );
  //     }
  //     throw error;
  //   }
  // }

  // @common.UseInterceptors(nestMorgan.MorganInterceptor("combined"))
  // @common.UseGuards(basicAuthGuard.BasicAuthGuard, nestAccessControl.ACGuard)
  // @common.Delete("/:id")
  // @nestAccessControl.UseRoles({
  //   resource: "healthCheck",
  //   action: "delete",
  //   possession: "any",
  // })
  // @swagger.ApiOkResponse({ type: healthCheck })
  // @swagger.ApiNotFoundResponse({ type: errors.NotFoundException })
  // @swagger.ApiForbiddenResponse({ type: errors.ForbiddenException })
  // async delete(
  //   @common.Param() params: healthCheckWhereUniqueInput
  // ): Promise<healthCheck | null> {
  //   try {
  //     return await this.service.delete({
  //       where: params,
  //       select: {
  //         createdAt: true,
  //         description: true,
  //         id: true,
  //         itemPrice: true,
  //         name: true,
  //         updatedAt: true,
  //       },
  //     });
  //   } catch (error) {
  //     if (isRecordNotFoundError(error)) {
  //       throw new errors.NotFoundException(
  //         `No resource was found for ${JSON.stringify(params)}`
  //       );
  //     }
  //     throw error;
  //   }
  // }

  // @common.UseInterceptors(nestMorgan.MorganInterceptor("combined"))
  // @common.UseGuards(basicAuthGuard.BasicAuthGuard, nestAccessControl.ACGuard)
  // @common.Get("/:id/orders")
  // @nestAccessControl.UseRoles({
  //   resource: "healthCheck",
  //   action: "read",
  //   possession: "any",
  // })
  // @swagger.ApiQuery({
  //   type: () => OrderWhereInput,
  //   style: "deepObject",
  //   explode: true,
  // })
  // async findManyOrders(
  //   @common.Req() request: Request,
  //   @common.Param() params: healthCheckWhereUniqueInput,
  //   @nestAccessControl.UserRoles() userRoles: string[]
  // ): Promise<Order[]> {
  //   const query: OrderWhereInput = request.query;
  //   const permission = this.rolesBuilder.permission({
  //     role: userRoles,
  //     action: "read",
  //     possession: "any",
  //     resource: "Order",
  //   });
  //   const results = await this.service.findOrders(params.id, {
  //     where: query,
  //     select: {
  //       createdAt: true,

  //       customer: {
  //         select: {
  //           id: true,
  //         },
  //       },

  //       discount: true,
  //       id: true,

  //       healthCheck: {
  //         select: {
  //           id: true,
  //         },
  //       },

  //       quantity: true,
  //       totalPrice: true,
  //       updatedAt: true,
  //     },
  //   });
  //   return results.map((result) => permission.filter(result));
  // }

  // @common.UseInterceptors(nestMorgan.MorganInterceptor("combined"))
  // @common.UseGuards(basicAuthGuard.BasicAuthGuard, nestAccessControl.ACGuard)
  // @common.Post("/:id/orders")
  // @nestAccessControl.UseRoles({
  //   resource: "healthCheck",
  //   action: "update",
  //   possession: "any",
  // })
  // async createOrders(
  //   @common.Param() params: healthCheckWhereUniqueInput,
  //   @common.Body() body: healthCheckWhereUniqueInput[],
  //   @nestAccessControl.UserRoles() userRoles: string[]
  // ): Promise<void> {
  //   const data = {
  //     orders: {
  //       connect: body,
  //     },
  //   };
  //   const permission = this.rolesBuilder.permission({
  //     role: userRoles,
  //     action: "update",
  //     possession: "any",
  //     resource: "healthCheck",
  //   });
  //   const invalidAttributes = abacUtil.getInvalidAttributes(permission, data);
  //   if (invalidAttributes.length) {
  //     const roles = userRoles
  //       .map((role: string) => JSON.stringify(role))
  //       .join(",");
  //     throw new common.ForbiddenException(
  //       `Updating the relationship: ${
  //         invalidAttributes[0]
  //       } of ${"healthCheck"} is forbidden for roles: ${roles}`
  //     );
  //   }
  //   await this.service.update({
  //     where: params,
  //     data,
  //     select: { id: true },
  //   });
  // }

  // @common.UseInterceptors(nestMorgan.MorganInterceptor("combined"))
  // @common.UseGuards(basicAuthGuard.BasicAuthGuard, nestAccessControl.ACGuard)
  // @common.Patch("/:id/orders")
  // @nestAccessControl.UseRoles({
  //   resource: "healthCheck",
  //   action: "update",
  //   possession: "any",
  // })
  // async updateOrders(
  //   @common.Param() params: healthCheckWhereUniqueInput,
  //   @common.Body() body: healthCheckWhereUniqueInput[],
  //   @nestAccessControl.UserRoles() userRoles: string[]
  // ): Promise<void> {
  //   const data = {
  //     orders: {
  //       set: body,
  //     },
  //   };
  //   const permission = this.rolesBuilder.permission({
  //     role: userRoles,
  //     action: "update",
  //     possession: "any",
  //     resource: "healthCheck",
  //   });
  //   const invalidAttributes = abacUtil.getInvalidAttributes(permission, data);
  //   if (invalidAttributes.length) {
  //     const roles = userRoles
  //       .map((role: string) => JSON.stringify(role))
  //       .join(",");
  //     throw new common.ForbiddenException(
  //       `Updating the relationship: ${
  //         invalidAttributes[0]
  //       } of ${"healthCheck"} is forbidden for roles: ${roles}`
  //     );
  //   }
  //   await this.service.update({
  //     where: params,
  //     data,
  //     select: { id: true },
  //   });
  // }

  // @common.UseInterceptors(nestMorgan.MorganInterceptor("combined"))
  // @common.UseGuards(basicAuthGuard.BasicAuthGuard, nestAccessControl.ACGuard)
  // @common.Delete("/:id/orders")
  // @nestAccessControl.UseRoles({
  //   resource: "healthCheck",
  //   action: "update",
  //   possession: "any",
  // })
  // async deleteOrders(
  //   @common.Param() params: healthCheckWhereUniqueInput,
  //   @common.Body() body: healthCheckWhereUniqueInput[],
  //   @nestAccessControl.UserRoles() userRoles: string[]
  // ): Promise<void> {
  //   const data = {
  //     orders: {
  //       disconnect: body,
  //     },
  //   };
  //   const permission = this.rolesBuilder.permission({
  //     role: userRoles,
  //     action: "update",
  //     possession: "any",
  //     resource: "healthCheck",
  //   });
  //   const invalidAttributes = abacUtil.getInvalidAttributes(permission, data);
  //   if (invalidAttributes.length) {
  //     const roles = userRoles
  //       .map((role: string) => JSON.stringify(role))
  //       .join(",");
  //     throw new common.ForbiddenException(
  //       `Updating the relationship: ${
  //         invalidAttributes[0]
  //       } of ${"healthCheck"} is forbidden for roles: ${roles}`
  //     );
  //   }
  //   await this.service.update({
  //     where: params,
  //     data,
  //     select: { id: true },
  //   });
  // }
}
