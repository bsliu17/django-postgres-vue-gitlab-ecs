import * as common from "@nestjs/common";
import * as graphql from "@nestjs/graphql";
import * as apollo from "apollo-server-express";
import * as nestAccessControl from "nest-access-control";
import * as gqlBasicAuthGuard from "../../auth/gqlBasicAuth.guard";
import * as gqlACGuard from "../../auth/gqlAC.guard";
import * as gqlUserRoles from "../../auth/gqlUserRoles.decorator";
import * as abacUtil from "../../auth/abac.util";
// import { isRecordNotFoundError } from "../../prisma.util";
// import { MetaQueryPayload } from "../../util/MetaQueryPayload";
// import { CreatehealthCheckArgs } from "./CreatehealthCheckArgs";
// import { UpdatehealthCheckArgs } from "./UpdatehealthCheckArgs";
// import { DeletehealthCheckArgs } from "./DeletehealthCheckArgs";
// import { healthCheckFindManyArgs } from "./healthCheckFindManyArgs";
// import { healthCheckFindUniqueArgs } from "./healthCheckFindUniqueArgs";
// import { healthCheck } from "./healthCheck";
// import { OrderFindManyArgs } from "../../order/base/OrderFindManyArgs";
// import { Order } from "../../order/base/Order";
import { HealthCheckService } from "../healthCheck.service";

// @graphql.Resolver(() => healthCheck)
// @common.UseGuards(gqlBasicAuthGuard.GqlBasicAuthGuard, gqlACGuard.GqlACGuard)
export class HealthCheckResolverBase {
  constructor(
    protected readonly service: HealthCheckService,
    protected readonly rolesBuilder: nestAccessControl.RolesBuilder
  ) {}

  // @graphql.Query(() => MetaQueryPayload)
  // // @nestAccessControl.UseRoles({
  // //   resource: "healthCheck",
  // //   action: "read",
  // //   possession: "any",
  // // })
  // async _healthChecksMeta(
  //   @graphql.Args() args: healthCheckFindManyArgs
  // ): Promise<MetaQueryPayload> {
  //   const results = await this.service.count({
  //     ...args,
  //     skip: undefined,
  //     take: undefined,
  //   });
  //   return {
  //     count: results,
  //   };
  // }

  @graphql.Query(() => Boolean)
  @nestAccessControl.UseRoles({
    resource: "healthCheck",
    action: "read",
    possession: "any",
  })
  async getHealthCheck(): Promise<boolean> {
    // const permission = this.rolesBuilder.permission({
    //   role: userRoles,
    //   action: "read",
    //   possession: "any",
    //   resource: "healthCheck",
    // });
    const results = await this.service.getHealthCheck();
    return results
  }

  // @graphql.Query(() => healthCheck, { nullable: true })
  // @nestAccessControl.UseRoles({
  //   resource: "healthCheck",
  //   action: "read",
  //   possession: "own",
  // })
  // async healthCheck(
  //   @graphql.Args() args: healthCheckFindUniqueArgs,
  //   @gqlUserRoles.UserRoles() userRoles: string[]
  // ): Promise<healthCheck | null> {
  //   const permission = this.rolesBuilder.permission({
  //     role: userRoles,
  //     action: "read",
  //     possession: "own",
  //     resource: "healthCheck",
  //   });
  //   const result = await this.service.findOne(args);
  //   if (result === null) {
  //     return null;
  //   }
  //   return permission.filter(result);
  // }

  // @graphql.Mutation(() => healthCheck)
  // @nestAccessControl.UseRoles({
  //   resource: "healthCheck",
  //   action: "create",
  //   possession: "any",
  // })
  // async createhealthCheck(
  //   @graphql.Args() args: CreatehealthCheckArgs,
  //   @gqlUserRoles.UserRoles() userRoles: string[]
  // ): Promise<healthCheck> {
  //   const permission = this.rolesBuilder.permission({
  //     role: userRoles,
  //     action: "create",
  //     possession: "any",
  //     resource: "healthCheck",
  //   });
  //   const invalidAttributes = abacUtil.getInvalidAttributes(
  //     permission,
  //     args.data
  //   );
  //   if (invalidAttributes.length) {
  //     const properties = invalidAttributes
  //       .map((attribute: string) => JSON.stringify(attribute))
  //       .join(", ");
  //     const roles = userRoles
  //       .map((role: string) => JSON.stringify(role))
  //       .join(",");
  //     throw new apollo.ApolloError(
  //       `providing the properties: ${properties} on ${"healthCheck"} creation is forbidden for roles: ${roles}`
  //     );
  //   }
  //   // @ts-ignore
  //   return await this.service.create({
  //     ...args,
  //     data: args.data,
  //   });
  // }

  // @graphql.Mutation(() => healthCheck)
  // @nestAccessControl.UseRoles({
  //   resource: "healthCheck",
  //   action: "update",
  //   possession: "any",
  // })
  // async updatehealthCheck(
  //   @graphql.Args() args: UpdatehealthCheckArgs,
  //   @gqlUserRoles.UserRoles() userRoles: string[]
  // ): Promise<healthCheck | null> {
  //   const permission = this.rolesBuilder.permission({
  //     role: userRoles,
  //     action: "update",
  //     possession: "any",
  //     resource: "healthCheck",
  //   });
  //   const invalidAttributes = abacUtil.getInvalidAttributes(
  //     permission,
  //     args.data
  //   );
  //   if (invalidAttributes.length) {
  //     const properties = invalidAttributes
  //       .map((attribute: string) => JSON.stringify(attribute))
  //       .join(", ");
  //     const roles = userRoles
  //       .map((role: string) => JSON.stringify(role))
  //       .join(",");
  //     throw new apollo.ApolloError(
  //       `providing the properties: ${properties} on ${"healthCheck"} update is forbidden for roles: ${roles}`
  //     );
  //   }
  //   try {
  //     // @ts-ignore
  //     return await this.service.update({
  //       ...args,
  //       data: args.data,
  //     });
  //   } catch (error) {
  //     if (isRecordNotFoundError(error)) {
  //       throw new apollo.ApolloError(
  //         `No resource was found for ${JSON.stringify(args.where)}`
  //       );
  //     }
  //     throw error;
  //   }
  // }

  // @graphql.Mutation(() => healthCheck)
  // @nestAccessControl.UseRoles({
  //   resource: "healthCheck",
  //   action: "delete",
  //   possession: "any",
  // })
  // async deletehealthCheck(
  //   @graphql.Args() args: DeletehealthCheckArgs
  // ): Promise<healthCheck | null> {
  //   try {
  //     // @ts-ignore
  //     return await this.service.delete(args);
  //   } catch (error) {
  //     if (isRecordNotFoundError(error)) {
  //       throw new apollo.ApolloError(
  //         `No resource was found for ${JSON.stringify(args.where)}`
  //       );
  //     }
  //     throw error;
  //   }
  // }

  // @graphql.ResolveField(() => [Order])
  // @nestAccessControl.UseRoles({
  //   resource: "healthCheck",
  //   action: "read",
  //   possession: "any",
  // })
  // async orders(
  //   @graphql.Parent() parent: healthCheck,
  //   @graphql.Args() args: OrderFindManyArgs,
  //   @gqlUserRoles.UserRoles() userRoles: string[]
  // ): Promise<Order[]> {
  //   const permission = this.rolesBuilder.permission({
  //     role: userRoles,
  //     action: "read",
  //     possession: "any",
  //     resource: "Order",
  //   });
  //   const results = await this.service.findOrders(parent.id, args);

  //   if (!results) {
  //     return [];
  //   }

  //   return results.map((result) => permission.filter(result));
  // }
}
