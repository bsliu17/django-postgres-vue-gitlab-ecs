import { Module } from "@nestjs/common";
import { HealthCheckModuleBase } from "./base/healthCheck.module.base";
import { HealthCheckService } from "./healthCheck.service";
import { HealthCheckController } from "./healthCheck.controller";
// import { HealthCheckResolver } from "./healthcheck.resolver";

@Module({
  imports: [HealthCheckModuleBase],
  controllers: [HealthCheckController],
  providers: [HealthCheckService],
  exports: [HealthCheckService],
})
export class HealthCheckModule {}
