#!/usr/bin/env python3
import os

from aws_cdk import core

from awscdk.app_stack import ApplicationStack

# BL: This is CDK stack.

# naming conventions, also used for ACM certs, DNS Records, resource naming
# Dynamically generated resource names created in CDK are used in GitLab CI
# such as cluster name, task definitions, etc.
environment_name = f"{os.environ.get('ENVIRONMENT', 'dev')}"
base_domain_name = os.environ.get("DOMAIN_NAME", "diamondfknhand.com")
# if the the production environent subdomain should nott be included in the URL
# redefine `full_domain_name` to `base_domain_name` for that environment
# dev.diamondfknhand.com
full_domain_name = f"{environment_name}.{base_domain_name}"
if environment_name == "app":
    full_domain_name = base_domain_name
base_app_name = os.environ.get("APP_NAME", "diamondfknhand-com")
full_app_name = f"{environment_name}-{base_app_name}"  # dev-diamondfknhand-com
aws_region = os.environ.get("AWS_DEFAULT_REGION", "us-west-2")
hosted_zone_id = os.environ.get("AWS_DEFAULT_REGION", "Z0634193LWHQCTDORM1S")

app = core.App()
stack = ApplicationStack(
    app,
    f"{full_app_name}-stackZZ2",
    environment_name=environment_name,
    base_domain_name=base_domain_name,
    full_domain_name=full_domain_name,
    base_app_name=base_app_name,
    full_app_name=full_app_name,
    env={"region": aws_region},
    hosted_zone_id=hosted_zone_id
)

# in order to be able to tag ECS resources, you need to go to
# the ECS Console > Account Settings > Amazon ECS ARN and resource ID settings
# and enable at least Service and Task. Optionally enable
# CloudWatch Container Insights
stack.node.apply_aspect(core.Tag("StackName", full_app_name))

app.synth()
