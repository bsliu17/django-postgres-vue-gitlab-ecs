import os

from aws_cdk import core, aws_ecs as ecs, aws_s3_deployment as s3_deployment, aws_ecr as ecr

from alb import AlbStack
from ts_alb import TsAlbStack
from backend_assets import BackendAssetsStack
from cert import SiteCertificate
from hosted_zone import HostedZone
from cloudfront import CloudFrontStack
from vpc import VpcStack
from rds import RdsStack
from elasticache import ElastiCacheStack
from ecs import EcsStack
from env_vars import Variables
from static_site_bucket import StaticSiteStack
from flower import FlowerServiceStack
from celery_autoscaling import CeleryAutoscalingStack
from bastion_host import BastionHost


from backend import BackendServiceStack
from ts_backend import TsServiceStack
from backend_tasks import BackendTasksStack
from celery_default import CeleryDefaultServiceStack


class ApplicationStack(core.Stack):
    def __init__(
        self,
        scope: core.Construct,
        id: str,
        environment_name: str,
        base_domain_name: str,
        full_domain_name: str,
        base_app_name: str,
        full_app_name: str,
        hosted_zone_id: str,
        **kwargs
    ) -> None:

        super().__init__(scope, id, **kwargs)

        self.environment_name = environment_name
        self.base_domain_name = base_domain_name
        self.full_domain_name = full_domain_name
        self.base_app_name = base_app_name
        self.full_app_name = full_app_name

        # BL: this script will
        # 1. create full_domain in the specified HostedZone (or i gotta create it)
        # 2. create SiteCert for the full domain but does not pass back the DNS validation CNAME key-value back to HostedZone.
        # I'll need to manually create CNAME
        self.hosted_zone = HostedZone(
            self, "HostedZone", hosted_zone_id=hosted_zone_id).hosted_zone
        self.certificate = SiteCertificate(self, "SiteCert")

        self.vpc_stack = VpcStack(self, "VpcStack")
        self.vpc = self.vpc_stack.vpc

        self.alb_stack = AlbStack(self, "AlbStack")
        self.alb = self.alb_stack.alb
        self.https_listener = self.alb_stack.https_listener

        self.ts_alb_stack = TsAlbStack(self, "TS-AlbStack")
        self.ts_alb = self.ts_alb_stack.ts_alb

        self.static_site_stack = StaticSiteStack(self, "StaticSiteStack")
        self.static_site_bucket = self.static_site_stack.static_site_bucket

        self.backend_assets = BackendAssetsStack(self, "BackendAssetsStack")
        self.backend_assets_bucket = self.backend_assets.assets_bucket

        # self.cloudfront = CloudFrontStack(self, "CloudFrontStackZZ")

        # if os.path.isdir("../quasar/dist/pwa"):
        #     s3_deployment.BucketDeployment(
        #         self,
        #         "BucketDeployment",
        #         destination_bucket=self.static_site_bucket,
        #         sources=[s3_deployment.Source.asset("../quasar/dist/pwa")],
        #         distribution=self.cloudfront.distribution,
        #     )

        # if os.path.isdir("../quasar/dist/spa"):
        #     s3_deployment.BucketDeployment(
        #         self,
        #         "BucketDeployment",
        #         destination_bucket=self.static_site_bucket,
        #         sources=[s3_deployment.Source.asset("../quasar/dist/spa")],
        #         distribution=self.cloudfront.distribution,
        #     )

        self.ecs = EcsStack(self, "EcsStack")
        self.cluster = self.ecs.cluster

        self.rds = RdsStack(self, "RdsStack")

        self.elasticache = ElastiCacheStack(self, "ElastiCacheStack")

        # image used for all django containers: gunicorn, daphne, celery, beat

        self.django_image = ecs.AssetImage(
            "../backend",
            file="scripts/dev/Dockerfile",
            target="development",
            # build_args={
            #     # 'platform': 'linux/amd64',
            #     'context': '.',
            # }
        )

        self.ts_image = ecs.AssetImage(
            "../ts",
            file="Dockerfile",
            target="builder",
            build_args={
                # 'platform': 'linux/amd64',
                'context': '.',
            }
        )

        self.variables = Variables(
            self,
            "Variables",
            bucket_name=self.backend_assets_bucket.bucket_name,
            db_secret=self.rds.db_secret,
            full_domain_name=self.full_domain_name,
            postgres_host=self.rds.rds_cluster.get_att(
                "Endpoint.Address"
            ).to_string(),
            redis_host=self.elasticache.elasticache.attr_redis_endpoint_address,  # noqa
        )

        self.backend_service = BackendServiceStack(self, "BackendServiceStack")

        self.ts_service = TsServiceStack(self, "TsServiceStack")

        # self.flower_service = FlowerServiceStack(self, "FlowerServiceStack")

        # self.celery_default_service = CeleryDefaultServiceStack(
        #     self, "CeleryDefaultServiceStack"
        # )

        # # define other celery queues here, or combine in a single construct

        # self.celery_autoscaling = CeleryAutoscalingStack(
        #     self, "CeleryAutoscalingStack"
        # )

        # # migrate, collectstatic, createsuperuser
        # self.backend_tasks = BackendTasksStack(self, "BackendTasksStack")

        # # # bastion host
        # self.bastion_host = BastionHost(self, "BastionHost")
